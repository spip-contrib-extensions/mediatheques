# Médiathèques

Plugin permettant de gérer des médiathèques thématisées, permettant de classer les documents, d'attribuer automatiquement une médiathèque à un document en fonction de règles définies par d'autres plugins dans un pipeline (selon le type d'objet, l'id_rubrique, etc)