<?php

function mediatheques_declarer_tables_principales($tables) {

	$tables['spip_mediatheques'] = [
		'editable' => true,
		'type' => 'mediatheques',
		'principale' => 'oui',
		//		'table_objet_surnoms' => array('instagram'),
		'field' => [
			'id_mediatheque' => 'bigint(21) NOT NULL',
			'titre' => 'text NOT NULL DEFAULT ""',
			'maj' => 'TIMESTAMP',
		],
		'key' => [
			'PRIMARY KEY' => 'id_mediatheque',
		],
	];

	return $tables;
}

function mediatheques_declarer_champs_extras($champs = []) {

	$champs['spip_documents']['id_mediatheque'] = [
		'options' =>
			[
				'nom' => 'id_mediatheque',
				'label' => 'Médiathèque',
				'sql' => 'bigint(21) NULL DEFAULT NULL',
				'versionner' => 'on',
			],
		'saisie' => 'mediatheques',
	];

	return $champs;
}