<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_filtrer_mediatheques_charger_dist($redirect = '') {
	include_spip('mediatheques_fonctions');
	$valeurs['id_mediatheque'] = _request('id_mediatheque');

	return $valeurs;
}

function formulaires_filtrer_mediatheques_traiter_dist($redirect = '') {
	$retours = [];
	if (!$redirect) {
		$redirect = self();
	}
	$redirect = parametre_url($redirect, 'id_mediatheque', _request('id_mediatheque'));
	$retours['redirect'] = $redirect;

	return $retours;
}