<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function mediatheques_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];

	// Créer la table
	$maj['create'] = [['maj_tables', ['spip_mediatheques']]];

	// Créer les champs extras
	include_spip('base/mediatheques');
	include_spip('inc/cextras');
	cextras_api_upgrade(mediatheques_declarer_champs_extras(), $maj['create']);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

function mediatheques_vider_tables($nom_meta_base_version) {
	sql_drop_table('spip_mediatheques');

	include_spip('base/upgrade');
	cextras_api_vider_tables(mediatheques_declarer_champs_extras());

	effacer_meta($nom_meta_base_version);
}
