<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function mediatheques_autoriser() {
}

function autoriser_document_modifierextra_id_mediatheque($faire, $type, $id, $qui, $opt) {
	return autoriser('webmestre', '', '', $qui);
}

function autoriser_document_voirextra_id_mediatheque($faire, $type, $id, $qui, $opt) {
	return false;
}

// TODO : autoriser modifier document selon id_mediatheque ?