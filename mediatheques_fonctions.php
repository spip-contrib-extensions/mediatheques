<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function id_mediatheque_document($id_document) {
	static $ids_mediatheques;
	if (isset($ids_mediatheques[$id_document])) {
		return $ids_mediatheques[$id_document];
	}
	$ids_mediatheques[$id_document] = sql_getfetsel('id_mediatheque', 'spip_documents', 'id_document = ' . $id_document);

	return $ids_mediatheques[$id_document];
}
