<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function mediatheques_post_edition_lien($flux) {
	include_spip('mediatheques_fonctions');
	// quand on ajoute un doc ou une image sur un objet, le lier à une éventuelle médiathèque
	if (
		$flux['args']['table_lien'] == 'spip_documents_liens'
		and $flux['args']['objet_source'] == 'document'
		and $id_document = $flux['args']['id_objet_source']
		and $objet = $flux['args']['objet']
		and $id_objet = $flux['args']['id_objet']
	) {
		// Demander aux plugins s'il y a une médiathèque à affecter
		$infos_mediatheque = pipeline(
			'mediatheque_par_objet',
			[
				'args' => [
					'objet' => $objet,
					'id_objet' => $id_objet,
				],
				'data' => [],
			]
		);
		if ($id_mediatheque = $infos_mediatheque['id_mediatheque']) {
			sql_updateq('spip_documents', ['id_mediatheque' => $id_mediatheque], 'id_document = ' . $id_document);
		}
	}

	return $flux;
}

function mediatheques_recuperer_fond($flux) {
	// ajouter un filtre Médiathèque
	if ($flux['args']['fond'] == 'prive/squelettes/inclure/mediatheque-navigation') {
		$fond = recuperer_fond('prive/squelettes/inclure/mediatheque-filtrer-mediatheques', $flux['args']['contexte']);
		$cherche = "#<ul\s+class=[\"']sanstitre[\"']>\s*(?:<li[^>]*>(?!.*<li>).*?</li>\s*)+\s*</ul>#i";
		$remplace = "$0$fond";
		$flux['data']['texte'] = preg_replace($cherche, $remplace, $flux['data']['texte']);
	}

	return $flux;
}

function mediatheques_pre_boucle($boucle) {
	if ($boucle->type_requete == 'documents') {
		$env_id = '$Pile[0]["id_mediatheque"]';
		$boucle->where[] =
			[
				sql_quote('?'), // si
				"(isset($env_id) && $env_id)", // condition (id_mediatheque dans l'env)
				[sql_quote('='), sql_quote('id_mediatheque'), $env_id], // condition where
				sql_quote(''), // sinon rien
			];
	}

	return $boucle;
}